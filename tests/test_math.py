import unittest
import math


class TestPythonFunctions(unittest.TestCase):
    
    def test_floor(self):
        self.assertEqual(math.floor(3.45), 3.0)

    
