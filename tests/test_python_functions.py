import unittest
import math


class TestPythonFunctions(unittest.TestCase):
    
    def test_floor(self):
        self.assertEqual(math.floor(3.45), 3.0)

    def test_upper(self):
        self.assertTrue("FOO".isupper())
        self.assertFalse("Foo".isupper())

    def test_split(self):
        msg = 'hello world'
        self.assertEqual(msg.split(), ['hello', 'world'])
        # check that s.split fails when the separator is not a string
        with self.assertRaises(TypeError):
            msg.split(2)

    def test_int_conversion(self):
        with self.assertRaises(ValueError):
            int(33)
            
if __name__ == '__main__':
    unittest.main()
