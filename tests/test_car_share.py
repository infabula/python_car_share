import unittest
import math

import car_share.car_share_service as css

class TestAdd(unittest.TestCase):
    
    def test_add(self):
        self.assertEqual(css.add(5, 5), 10)
