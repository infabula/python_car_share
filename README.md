# Virtual environment
Create a virtual environment with the name cs_venv:

python -m venv cs_venv

and activate:
(linux)
source cs_venv/bin/activate

(windows)
cs_venv\Scripts\activate

Install the dependency packages:

pip install -r requirements.txt

# git pre-commit hook
To automatically run tests before a commit:

cd .git/hooks
ln -s ../../pre-commit.sh ./pre-commit

To temporarily bypass the pre-commit hook:

git commit --no-verify  -am "commit message"
